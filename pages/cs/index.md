---
title: MČR HROB 2016 - News
---

<table>
<tr><td> 5.&nbsp;9.&nbsp;2016 </td> <td>
Krušnohorský rogainingový klub začal připravovat MČR v horském orientačním běhu. Uskuteční se 5. až 6. listopadu 2016 v Klínech (okres Most).

Dnes v poklusu mezi mapováním a vyřizováním povolení spouštíme web závodu. Můžete si přečíst [rozpis závodu](rozpis-hrob-2016.html), sledovat [informace o počasí](pocasi-hrob-2016.html), prohlédnout si [fotky z terénu](fotky-hrob-2016.html) a hlavně se přihlašovat pomocí [přihláškového systému](http://hrob2016.rogaining.cz).
</td> </tr>
</table>
