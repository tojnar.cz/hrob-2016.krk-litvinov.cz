---
title: Fotogalerie MČR HROB 2016
---

#### Fotky z přípravy závodu

* [Listopad 2012](https://goo.gl/photos/pvACCQ4yG14mgmJb7)
* [Listopad 2013](https://goo.gl/photos/Wpav612wFu6qNDPT7)
* [Probíhání 3.-4. 9. 2016](gallery_09_03-04.html)
