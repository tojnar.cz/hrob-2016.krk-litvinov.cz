---
title: MČR HROB 2016 - Rozpis
---

<table border="1" cellspacing="1" cellpadding="1" style="width:100%;" class="c9" bordercolor="#C0C0C0">
<tbody>
<tr>
<td style="width:21%;">
<center>[![Logo ČAR](/images/logo-car.svg){height="120"}](http://www.rogaining.cz/)</center>
</td>
<td colspan="3" style="width:58%;" align="justify">
<center><h4>Rozpis Mistrovství České republiky v horském orientačním běhu 2016</h4></center>
</td>

<td style="width:21%;">
<center>[![Logo ČAES](/images/logo-caes.svg){height="120"}](http://www.caes.cz)
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Pořádající orgán:**
</td>
<td colspan="4" style="width:79%;">
[Česká asociace rogainingu a horského orientačního běhu (ČAR)](http://www.rogaining.cz/)
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Pořádající subjekt:**
</td>
<td colspan="4" style="width:79%;">
[Krušnohorský rogainingový klub Litvínov](http://www.krk-litvinov.cz/)
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Datum:**
</td>
<td colspan="4" style="width:79%;">
**5.–6. 11. 2016**
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Místo:**
</td>
<td colspan="4" style="width:79%;">
**Klíny v&nbsp;Krušných horách**, okres Most
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Centrum:**
</td>
<td colspan="4" style="width:79%;">
**[Hotel Emeran](http://www.hotelemeran.cz/)**, **[poloha na outdoormapa.cz](http://www.outdoormapa.cz/link.php?x=50.6300&y=13.5599&z=17&_ga=1.184343206.1496328656.1423651155)**
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Druh závodu:**
</td>
<td colspan="4" style="width:79%;">
**Dvouetapový horský orientační běh dvojic** (kombinace volitelného a pevného pořadí kontrol).
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Prostor závodu a terén:**
</td>
<td colspan="4" style="width:79%;">
V sobotu svahy Krušných hor v okolí Klínů, v neděli navíc pseudoskandinávské terény na výsypce v podhůří. Nadmořská výška kontrol nevybočí z intervalu 240 až 950&nbsp;m.&nbsp;n.&nbsp;m.
Terén převážně rovinatý, rovina je ovšem velmi často značně nakloněná :-). Většinu prostoru závodu pokrývá horský či podhorský les se středně hustou sítí komunikací, v neděli navíc uměle osázené kypy Jiřetínské výsypky.
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Mapa:**
</td>
<td colspan="4" style="width:79%;">
Speciálka pro HROB, formát A4, měřítko 1:25 000, e=10&nbsp;m, stav 10/2016, vodovzdorný materiál Pretex + A4 eurodesky k dispozici na startu
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Vypsané kategorie:**
</td>
<td colspan="4" style="width:79%;">
* HH (dvojice mužů, bez omezení věku)
* DD (dvojice žen, bez omezení věku)
* HD (smíšená dvojice, bez omezení věku)
* HH40 (dvojice mužů, nar. před 5. 11. 1976)
* DD40 (dvojice žen, nar. před 5. 11. 1976)
* HD40 (smíšená dvojice, nar. před 5. 11. 1976)
* HH20 (dvojice mužů, nar. po 5. 11. 1996)
* DD20 (dvojice žen, nar. po 5. 11. 1996)
* HD20 (smíšená dvojice, nar. po 5. 11. 1998)
* PO (příchozí orienťáci bez omezení věku a pohlaví, orientačně obtížná trať, nemistrovská kategorie s možností přihlášení pouze na jednu etapu)
* PZ (příchozí začátečníci bez omezení věku a pohlaví, orientačně jednoduchá trať, nemistrovská kategorie s možností přihlášení pouze na jednu etapu)
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Podmínky účasti:**
</td>
<td colspan="4" style="width:79%;">
V závodě MČR smí startovat jen členové České asociace rogainingu (ČAR) starší 15 let. Do ČAR je možno bezplatně vstoupit na prezentaci.

Závodníci mladší 15 let mohou startovat pouze v kategorii příchozích, nebo zažádat o výjimku, kterou posoudí prezidium ČAR. Na prezentaci též musí doložit písemný souhlas svého zákonného zástupce,
stáhněte si [formulář](/files/souhlas.pdf).

Oba členové týmu musí absolvovat celou trať pohromadě. Při vzdání kteréhokoli z nich se musí kompletní tým dostavit do cíle a závod společně ukončit.
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Ubytování:**
</td>
<td colspan="4" style="width:79%;">
* Prostřednictvím přihláškového systému lze za 120 Kč/os. a noc objednat pořadatelské ubytování na podlaze [sportovní haly](http://www.kliny.cz/leto/letni-aktivity/multifunkcni-sportovni-hala) přímo v centru závodu (vlastní spacák, karimatka, přezouvá se!!!).
* Při včasné rezervaci (bez asistence pořadatelů) je možné se ubytovat v některém z ubytovacích zařízení [Sportareálu Klíny](http://www.kliny.cz/leto/ubytovani/).
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Přihlášky a startovné:**
</td>
<td colspan="4" style="width:79%;">
**[On-line přihláškovým systémem](/entries)** na webu závodu. Vaše přihláška je zaregistrována v okamžiku, kdy obdržíte potvrzení (reply).

**Startovné zaplacené v řádném termínu** (do&nbsp;neděle 16. 10. 2016 24:00 hod.):

* Příchozí 300 Kč za dvojici
* Ostatní 600 Kč za dvojici

**Startovné zaplacené po řádném termínu** (od&nbsp;pondělí 17. 10. 2016 až do skončení prezentace, omezeně jen dle možností pořadatele):

* Příchozí 400 Kč za dvojici
* Ostatní 750 Kč za dvojici			 
</td>
</tr>									
<tr>
<td style="width:21%;" valign="top">
**Způsob platby:**
</td>
<td colspan="4" style="width:79%;">
Vložné, tzn. startovné + nocležné, popř. poplatek za půjčení čipu, zašlete prosím bankovním převodem na FIO konto Krušnohorského rogainingového klubu Litvínov číslo: **2800039826/2010**<br>
Jako variabilní symbol platby uvádějte prosím ID vašeho týmu, které obdržíte v mailu, potvrzujícím přijetí vaší přihlášky.<br>
Kopii dokladu o zaplacení předložte na vyžádání při prezentaci (týká se zejména pozdních plateb).
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Doprava a parkování:**
</td>
<td colspan="4" style="width:79%;">
* Z Prahy autobusem nebo vlakem přes Ústí nad Labem/Teplice do Litvínova, přestup na autobus směr Český Jiřetín, cílová zastávka Klíny, hostinec
* Vlastními dopravními prostředky: parkujte bezplatně na parkovištích P1 a P2, která jsou v zimě určena pro lyžaře. Všude jinde v obci vám hrozí odtažení vozidla! Dbejte pokynů pořadatelů a nenechávejte příjezd na poslední chvíli.
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Prezentace:**
</td>
<td colspan="4" style="width:79%;" valign="top">
V **pátek** 4. 11. 2016 od **19:00 do 22:00** a v&nbsp;**sobotu** 5. 11. 2016 mezi **8:00 až 9:00** v **hotelu Emeran** na Klínech.

Na prezentaci doporučujeme přijet již v pátek. Všichni startující zde musí podepsat prohlášení, že startují na vlastní nebezpečí. Za účastníky mladší 18 let toto prohlášení podepíše jejich rodič nebo zákonný zástupce.
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Průběh závodu:**
</td>
<td colspan="4" style="width:79%;" valign="top">
**Sobota 5.&nbsp;11.&nbsp;2016:**<br>
**10:00** hromadný start ve vlnách, free order + trať s pevným pořadím kontrol; časový limit 6 hod. 30 min.

**Neděle 6.&nbsp;11.&nbsp;2016:**<br>
Trať s pevným pořadím kontrol a časovým limitem 6 hod. 30 min. Na start druhé etapy plánujeme odvoz autobusy. Týmy se ztrátou do 30 minut na vítěze odstartují v hendikepovém startu, začátek v **9:30**. Ostatní hromadně ve vlnách, detaily upřesněníme v pokynech.
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Časomíra a systém ražení:**
</td>
<td colspan="4" style="width:79%;">
Bude použit systém SportIdent. **!!! Každý člen týmu poběží s vlastním SI čipem !!!**<br>
SI čip si můžete zapůjčit u pořadatele na prezentaci, cena 60 Kč/ks. !!! Při ztrátě čipu zaplatíte částku 1000,- Kč !!!
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Občerstvení:**
</td>
<td colspan="4" style="width:79%;">
Na trati pořadatel zajistí 1 až 2 občerstvovací kontroly. V prostoru závodu se nachází několik studánek/pramenů (v mapě vyznačeny modrým kelímkem). Vodu z většiny potoků tekoucích mimo obydlené oblasti při tréninku běžně bez následků piji, nicméně záruku, že i vaše zažívací ústrojí vše zvládne bez následků, ode mne nečekejte :-). Letošní sucho navíc leckterou bystřinu proměnilo ve vysychající vodoteč, doporučuji pít pouze z opravdu zurčících potůčků.
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Funkcionáři:**
</td>
<td colspan="4" style="width:79%;">
Ředitel závodu: **Jan Tojnar** <br>
Hlavní rozhodčí: **Pavel Kurfürst** <br>
Stavba tratí: **Jan Tojnar plus kibicové**
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Informace:**
</td>
<td colspan="4" style="width:79%;">
Na webu závodu: <a href="http://hrob-2016.krk-litvinov.cz">**http://hrob-2016.krk-litvinov.cz**</a>
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Upozornění:**
</td>
<td colspan="4" style="width:79%;">
<ul>
<li>Závodí se podle platných <a href="http://www.rogaining.cz/upload/CAR_pravidla_2012.pdf">pravidel ČAR</a>.
<li>Každý závodník se akce účastní na&nbsp;vlastní nebezpečí a sám zodpovídá za&nbsp;svou bezpečnost. Pořadatel neručí za&nbsp;žádné škody způsobené účastníky akce.</li>
</ul>
</td>
</tr>
<tr>
<td style="width:21%;" valign="top">
**Schvalovací doložka:**
</td>
<td colspan="4" style="width:79%;">
Rozpis byl schválen Prezidiem ČAR v&nbsp;září 2016.
</td>
</tr>
</tbody>
</table>
