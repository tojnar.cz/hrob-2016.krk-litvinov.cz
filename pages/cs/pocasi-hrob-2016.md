---
title: Informace o počasí v závodním prostoru
---

### Předpověď počasí Klíny (725 m. n. m.)

* [Norská předpověď pro Klíny](http://www.yr.no/place/Czech_Republic/%C3%9Ast%C3%AD_nad_Labem/Kl%C3%ADny/)
* [Foreca weather Klíny](http://www.foreca.cz/Czech_Republic/Kliny)
* [Webkamery Sportareálu Klíny](http://www.kliny.cz/webkamery)
